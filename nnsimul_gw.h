#ifndef __NNSIMUL_GW_H__
#define __NNSIMUL_GW_H__

#include "nnsimul.h"

#include "api_scilab.h"
#include "Scierror.h"

#if SCI_VERSION_MAJOR >= 6
#define GATEWAY_PARAMETERS char* fname, void *pvApiCtx
#else
#define GATEWAY_PARAMETERS char* fname, unsigned long fname_len
#endif

#if SCI_VERSION_MAJOR >= 6
#include "sci_malloc.h"
#else
#include "MALLOC.h"
#endif

#endif