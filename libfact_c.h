#ifndef __LIBFACT_C_GW_H__
#define __LIBFACT_C_GW_H__

#include "c_gateway_prototype.h"

STACK_GATEWAY_PROTOTYPE(sci_nnsimul);
STACK_GATEWAY_PROTOTYPE(sci_nnsimulbis);
STACK_GATEWAY_PROTOTYPE(sci_nnsimulter);

#endif /* __LIBFACT_C_GW_H__ */
