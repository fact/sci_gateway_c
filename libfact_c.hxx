#ifndef __LIBFACT_C_GW_HXX__
#define __LIBFACT_C_GW_HXX__

#ifdef _MSC_VER
#ifdef LIBFACT_C_GW_EXPORTS
#define LIBFACT_C_GW_IMPEXP __declspec(dllexport)
#else
#define LIBFACT_C_GW_IMPEXP __declspec(dllimport)
#endif
#else
#define LIBFACT_C_GW_IMPEXP
#endif

extern "C" LIBFACT_C_GW_IMPEXP int libfact_c(wchar_t* _pwstFuncName);



#endif /* __LIBFACT_C_GW_HXX__ */
